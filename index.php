<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html class="no-js" lang="ru-Ru">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Test exercise</title>
	<meta name="description" content="lanet website">

	<!-- make here a fallback to favicon if old IE browsers -->

	<link rel="icon" href="/favicon.ico" sizes="16x16" type="image/x-icon" />

	<!-- make here an support of html5 features for old IE browsers -->

	<link rel="stylesheet" href="css/main.css">

	<script>
		"use strict";

			// create the repeat function, to append the text "Kiev" from photo caption 3 times,
			// but not modify the code below:
			var text=$('.image').text();
			$('.image').append(text.repeat(3));

			var city = 'Kiev';
			function test() {
				console.log(a);
				var a = 2;
				function foo() {
					return 2;
				}
			}
			test();// edit the function above to console.log  "2" as result of test() function


			var cities = {
				city: 'Kiev',
				data: {
					city: 'Lviv',
					getCity: function() {
						return this.city;
					}
				}
			};

			console.log(cities.data.getCity());
			var get_city = cities.data.getCity;
			// console.log "Lviv" as result of get_city() function without modifying code above




	</script>

</head>
<body ng-controller="mainCtrl">
<!--  improve accessibility using WAI-ARIA roles where appropriate:  -->
<header>
	<h1>Main title</h1>
	<!--  make ajax request to show filtered places by search term  -->
	<form action="assets/search.php" method="put">
		<input type="search" ng-model="search_criteria">
		<button type="submit">search</button>
	</form>
	<ul>
		<li><a href="/">Home</a></li>
		<li><a href="/guide">City guide</a></li>
		<li><a href="/about">About</a></li>
	</ul>
</header>

<main>
	<article>
		<time datetime="29.04.2015-30.04.2015">29-30 April 2015</time>
		<h1 ng-bind="city"></h1>
		<p>This is the beautiful photo of Kiev</p>
		<div class="image">
			<figure>
				<picture>
					<source media="(min-width: 40em)"
					        srcset="images/xlarge.jpg  1024w,images/medium.jpg 640w,images/small.jpg  320y"
					        sizes="60vw" />
					<img src="images/medium.jpg" alt="{{city}} photo">
				</picture>
				<figcaption>{{city}}</figcaption>
			</figure>
		</div>
		<div id="search_results" ng-model="search_results"></div>
	</article>
</main>
<footer>
	<small>Lanet &copy; 2015 </small>
</footer>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-route.min.js"></script>
<script src="js/app.js"></script>
<!-- make here a fallback to static jQuery, from projects js folder -->


</body>
</html>