Create a php script using provided places.csv file, which should:

-create a table in MySQL with places.csv structure.
-all data from CSV-file export to created table.
-process an ajax request from website with search term and return results.
