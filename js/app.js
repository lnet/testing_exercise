"use strict";

//---------------------------------------------//
//                                             //
//                 MAIN APP                    //
//                                             //
//---------------------------------------------//
var app = angular.module('Exercise',['ngRoute']);

// create routing to:
// 1. homepage
// 2. guide page
// 3. about page

angular.element(document).ready(function () {
// bootstrap your app
});
app.controller('mainCtrl',['$scope',function($scope) {
	$scope.city='Kiev';
}]);

